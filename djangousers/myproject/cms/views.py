from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
import random
# Create your views here

formulario = """"
No existe valor en la base de datos para esta llave.
<p>Introducela:
<form action= "" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""
@csrf_exempt
def get_content(request, llave):

    if request.method in ['POST', 'PUT']:
        if request.method == 'POST':
        #si es post lo tomo del body y llo guardo
            valor = request.POST['valor']
        else:
            #si es put decodifico la clave que se pasa en el cuerpo de la solicitud
            valor = request.body.decode('utf-8')
            try:
                #si la clave existe se elimina porque en le cuerpo d ela solicitud definimo sun nuevo valor
                if request.user.is_authenticated:
                    contenido = Contenido.objects.get(clave=llave)
                    contenido.delete()
                else:
                    respuesta = "No estas autenticado. <a href='/login'>Haz login!</a>"
                    return HttpResponse(respuesta)
                #si el usuario esta auetnticado le tengo qu emostrar una opcion de modificar la clave qque se pasa

            except Contenido.DoesNotExist:
                pass


        contenido = Contenido(clave=llave, valor=valor)
        #lo guardo en la base de datos
        contenido.save()

    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = (
                "La llave " + contenido.clave + " tiene el valor " + contenido.valor + " y el id " + str(
            contenido.id) )
        #devuelve un objeto no varios
    except Contenido.DoesNotExist: #si no existe es porque hgas hecho un post entonces se comprueba si esta s registrado
        if request.user.is_authenticated:
            respuesta = formulario
        else:
            respuesta = "No estas autenticado. <a href='/login'>Haz login!</a>"
        #ahora lo que queremos es que solo lo vea alguien que este autenticado igual que hace la interfaz de admin pero para nuestra api
    return HttpResponse(respuesta)



#pagina html
def index(request):
    #1. Tenr la lista de contenidos
    content_list = Contenido.objects.all()
    #2. Cargar la plantilla
    colores_html = ['yellow', 'blue', 'white', 'gold', 'gray', 'brown',
                    'purple', 'orange', 'black', 'silver', 'red', 'pink', 'green']
    color_color = random.choice(colores_html)
    color_background = random.choice(colores_html)
    css_template = """
            body {
        margin: 10px 20% 50px 70px;
        font-family: sans-serif;
        color: """ + color_color + """;
        background: """ + color_background + """;
        }"""



    template = loader.get_template('cms/index.html')
    #3. Ligar las variables de la plantilla a las variables de Python
    context = {'content_list': content_list, 'css_content': css_template}
    #4 Renderizar.... es ejecutar 3
    return HttpResponse(template.render(context, request))
#aplicar el contexto a la plantilla

#vamos a añadir una lisa para comporibar si alguien ha hecho un login.html

def loggedIn(request):

    if request.user.is_authenticated: #ver si el usuario esta autenticado para imprimir su nombre
        respuesta = 'Logged in as ' + request.user.username #depende de quien nos hace la peticion
    else:
        respuesta = "No estas autenticado. <a href='/login'>Autenticate</a>"
    return HttpResponse(respuesta)

def logout_view(request):
    logout(request)
    return redirect('/cms/')


